package composite;

import java.util.ArrayList;
import java.util.List;

public class GrupoCompuesto extends IntegranteComponente {
	private List<IntegranteComponente> lstIntegranteComponente;

	public GrupoCompuesto(String nombre) {
		super(nombre);
		// TODO Auto-generated constructor stub
		this.setLstIntegranteComponente(new ArrayList<IntegranteComponente>());
	}

	@Override
	public String usarIntrumento() {
		String cancion = "\nGrupoCompuesto: "+nombre;
		for(IntegranteComponente integrante:this.getLstIntegranteComponente()){
			cancion = cancion.concat(integrante.usarIntrumento().concat(" "));
		}
		return cancion;
	}

	public List<IntegranteComponente> getLstIntegranteComponente() {
		return lstIntegranteComponente;
	}

	public void setLstIntegranteComponente(List<IntegranteComponente> lstIntegranteComponente) {
		this.lstIntegranteComponente = lstIntegranteComponente;
	}

	public void addIntegranteComponente(IntegranteComponente integrante){
		this.getLstIntegranteComponente().add(integrante);
	}

	public void removeIntegranteComponente(IntegranteComponente integrante) {
		this.getLstIntegranteComponente().remove(integrante);
	}
}
